import { Component, OnInit, Input } from '@angular/core';
import { GoogleMapsAPIWrapper } from '@agm/core';
import {IOption} from 'ng-select';
import { PharmaciesService } from '../services/pharmacies.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  providers :[GoogleMapsAPIWrapper,PharmaciesService],
  styleUrls: [
  './accueil.component.css'
]
})

export class AccueilComponent implements OnInit {

  Currentlat: number ;
  Currentlng: number ;
  pharmacies = [];
  pharmacySearch = []
  myOptions: Array<IOption> = [];


  constructor(private  pharmaciesService : PharmaciesService) { 

    
    pharmaciesService.getAllPharmacies().subscribe( 
      res =>    {
        this.pharmacies = res
        this.pharmacySearch = [...res ];        
        res.forEach(pharmacie => {
          this.myOptions =[ ...this.myOptions,{ label: pharmacie.name , value: pharmacie.name } ];
          
        });
      } ,
      err => console.log(err)
    )
    
    
    
    
  }

  ngOnInit() {
    this.getUserLocation();
  }

  private getUserLocation(){
    //locate the user
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.Currentlat = position.coords.latitude;
        this.Currentlng = position.coords.longitude;
      });
    }
  }

  currentLocation(pharmaLocationCoords){
    //locate the user
    this.pharmacies.forEach(pharmacie => {
        if (pharmaLocationCoords === pharmacie.name) {
            this.Currentlat = pharmacie.latitude;
            this.Currentlng = pharmacie.longitude;
        }
      });

} 

}
