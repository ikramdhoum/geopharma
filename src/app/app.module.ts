import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { HeaderComponent } from './header/header.component';
import { CommandeComponent } from './commande/commande.component';
import { FooterComponent } from './footer/footer.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PanierComponent } from './panier/panier.component';
import { ResumeCommandeComponent } from './resume-commande/resume-commande.component';
import { ShopCheckoutComponent } from './shop-checkout/shop-checkout.component';
import { ClientComponent } from './client/client.component';
import { GeoService } from './geo.service';
import { RouterModule } from "@angular/router";
import { SelectModule } from 'ng-select';
// imports Geofire 
import { environment }  from '../environments/environment';
//import { AngularFireModule } from 'angularfire2';
export const firebaseConfig  = environment.firebaseConfig;

import { AgmCoreModule } from "@agm/core";
import { GoogleMapComponent } from './google-map/google-map.component';
import { RegisterComponent } from './register/register.component';
import { ShowErrorsComponent } from './show-errors/show-errors.component';
import { PharmacieComponent } from './pharmacie/pharmacie.component';


@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    SearchResultComponent,
    HeaderComponent,
    CommandeComponent,
    FooterComponent,
    PanierComponent,
    ResumeCommandeComponent,
    ShopCheckoutComponent,
    ClientComponent,
    GoogleMapComponent,
    RegisterComponent,
    ShowErrorsComponent,
    PharmacieComponent
  ],
  imports: [
    BrowserModule,
    SelectModule,
    FormsModule,
    HttpModule,
    //AngularFireModule.initializeApp(firebaseConfig),
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsKey
    }),
    NgxPaginationModule,
    RouterModule.forRoot([
      {
        path : '',
        component : AccueilComponent
      },
      {
        path : 'accueil',
        component : AccueilComponent
      },
      {
        path : 'search/pharmacie/:id',
        component : CommandeComponent
      },
      {
        path : 'client/dashboard',
        component : ClientComponent
      },
      {
        path : 'search',
        component : SearchResultComponent
      },
      {
        path : 'pharmacie/:id/commande/checkout',
        component : ShopCheckoutComponent
      },
      {
        path : 'commande/resume',
        component : ResumeCommandeComponent
      },
      {
        path : 'commande/resume#openModel',
        component : ResumeCommandeComponent
      },
      {
        path : 'register',
        component : RegisterComponent
      },
      {
        path : 'pharmacie/dashboard',
        component : PharmacieComponent
      },
      {
        path : 'pharmacie/:id',
        component : CommandeComponent
      }

    ])
  ],
  providers: [GeoService],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
