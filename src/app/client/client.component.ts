import {Component, OnInit} from '@angular/core';
import {UsersService} from '../services/users.service';
import {CommandesService} from '../services/commandes.service';
import { environment } from '../../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';

@Component({selector: 'app-client', templateUrl: './client.component.html', styleUrls: ['./client.component.css'],providers : [UsersService,CommandesService]})
export class ClientComponent implements OnInit {
  profile; 
  orders;
  path = environment.host;
  constructor(private usersService : UsersService, public sanitizer: DomSanitizer, private commandeService : CommandesService) {
    usersService
      .getProfile(localStorage.getItem('token'))
      .subscribe(res =>{ this.profile = res}, err => console.log(err))
    commandeService.clientOrders().subscribe(res => {
      console.log(res);
      this.orders = res});
  }

  ngOnInit() {}
  actionOnSubmit(f : NgForm) {
    let values = f.value;
    console.log(values);
    this.usersService.updateProfile(values).subscribe(
      res => {
        window.location.replace("http://localhost:4200/client/dashboard");

      },
      err=> console.log(err)
    )
  }
}
