import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Router} from "@angular/router";
import { DomSanitizer } from '@angular/platform-browser';
import { PharmaciesService } from '../services/pharmacies.service';
import { UsersService } from '../services/users.service';
import { CommandesService } from '../services/commandes.service';
 


@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.css'],
  providers : [PharmaciesService,UsersService,CommandesService]
})
export class CommandeComponent implements OnInit {

  title = "portail pharmacie";
  cartArticles = [];
  cartArticleItem = {};
  pharmacieId ;
  filtredMeds;
  checkeMedicament = [];
  medicaments = [];
  subtotal ;
  total = 0;
  pharmacie = {};
showFeatures = false;
  constructor(private ActivatedRoute: ActivatedRoute , public sanitizer: DomSanitizer, 
    private  pharmaciesService : PharmaciesService , private usersService : UsersService, private commandesService : CommandesService , private router : Router) { 
    
    this.showFeatures = usersService.isAuthenticated();
    console.log(this.showFeatures);
    this.ActivatedRoute.paramMap.subscribe(params => {
      this.pharmaciesService.getPharmacie(params.get('id')).subscribe(
        res => this.pharmacie = res , 
        err => console.log(err) 
      );
      
      this.pharmaciesService.getAllMedicamentsOfPharmacie(params.get('id')).subscribe(
        res => {
          console.log(res);
          this.medicaments = res ,
          this.filtredMeds = res;
          res.forEach(medicament => {
            this.checkeMedicament[medicament.name] = false;
          });
        }  
          , 
        err => console.log(err)
      )
      this.pharmacieId  = params.get('id');
    });

  }

  ngOnInit() {
    this.getPharmacyById(this.pharmacieId);
  }

  getPharmacyById(pharmacyId){

  }

  addToCart(medicament){
    this.checkeMedicament[medicament.name] = true;
    this.cartArticles.push({...medicament , subtotal:medicament.price});
    console.log(this.cartArticles)
    this.total = this.cartArticles.reduce((a,b)=> a+b.subtotal,0);
    console.log(this.total);
  }

  deleteFromCart(medicament){
    this.checkeMedicament[medicament.name] = false;
    this.cartArticles = this.cartArticles.filter(article => article.id != medicament.id);
    this.total = this.cartArticles.reduce((a,b)=> a+b.subtotal,0);
  }

  onKeyUp(value : string) {
   this.filtredMeds = this.medicaments.filter(medicament => medicament.name.toUpperCase().includes(value.toUpperCase()));
  }

  calculateSubTolal(article , price, qte,qteField){

   if(qteField.value>article.quantity) qteField.value;
    article.subtotal = price*qte ;
    article.currentQte = parseInt(qte);
    this.total = this.cartArticles.reduce((a,b)=> a+b.subtotal,0);
  }

  checkout() {
    let orders = [{pharmacieId : 0 , medicamentQtes : []}  ];
    orders[0].pharmacieId = this.pharmacieId;
    orders[0].medicamentQtes = this.cartArticles.map( o => {return { id : o.id , quantity : o.quantity}})
    this.commandesService.checkout(orders).subscribe(
      res => {
        this.router.navigate(['/client/dashboard']);
      },
      err => console.log(err)
    );

  }

}
