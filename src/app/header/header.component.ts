import { Component, OnInit, Input } from '@angular/core';
import { CommandeComponent } from '../commande/commande.component';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers : [UsersService]
})
export class HeaderComponent implements OnInit {

  @Input() medicaments : CommandeComponent;
  @Input() cartArticle : CommandeComponent;
  @Input() total : CommandeComponent;
  @Input() deleteFromCart : Function;

  
  constructor(private usersService : UsersService) {

   }

   logout() {
     this.usersService.logout();
   }

  ngOnInit() {
  }

}
