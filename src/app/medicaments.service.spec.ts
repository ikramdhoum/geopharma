import { TestBed, inject } from '@angular/core/testing';

import { MedicamentsService } from './services/medicaments.service';

describe('MedicamentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MedicamentsService]
    });
  });

  it('should be created', inject([MedicamentsService], (service: MedicamentsService) => {
    expect(service).toBeTruthy();
  }));
});
