import { Component, OnInit, Input } from '@angular/core';
import { CommandeComponent } from '../commande/commande.component';


@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  @Input() cartArticles;
  @Input() total : number;
  @Input() checkeMedicament;
  @Input() pharmacieId;
  @Input() title : string;

  constructor() {   
  }

  deleteFromCart(medicament){
    this.checkeMedicament[medicament.name] = false;
    this.cartArticles = this.cartArticles.filter(article => article.id != medicament.id);
    this.total = this.cartArticles.reduce((a,b)=> a+b.subtotal,0);
  }

  ngOnInit() {
    
  }
}
