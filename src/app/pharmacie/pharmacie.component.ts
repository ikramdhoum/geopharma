import { Component, OnInit } from '@angular/core';
import { GoogleMapsAPIWrapper } from '@agm/core';
import { FormGroup, FormControl, FormArray, NgForm } from '@angular/forms';
import { MedicamentsService } from '../services/medicaments.service';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';
import { CommandesService } from '../services/commandes.service';

@Component({
  selector: 'app-pharmacie',
  providers : [MedicamentsService,UsersService, CommandesService],
  templateUrl: './pharmacie.component.html',
  styleUrls: ['./pharmacie.component.css']
})
export class PharmacieComponent implements OnInit {

  title = "dashboard";
  medicaments = [];
  filtredMeds: any[];
  checkeMedicament: any;

  isOpen= true;
  commandes: any[];
  listMedCommande: any;
  commandeMeds = [];
  filtredCmd: any[];
  pharmacie_profile ;
  pharmacieId;

// map variables
  lat: any;
  lng: any;
  currentLat: any;
  currentLng: any;
  medicamentId;

  constructor(private medicamentsService : MedicamentsService, private router : Router , private usersService : UsersService , private commandeService : CommandesService) {
    this.commandeService.pharmacieOrders().subscribe(
      res => {this.commandeMeds = res;
        this.commandes = res;
        console.log(res);
      this.filtredCmd = res},
      err => console.log(err)
    )
   this.usersService.getProfile(localStorage.getItem("token")).subscribe(
     res => {
        let medicaments = this.medicamentsService.gtMedicamentByPharmacie(res.pharmacie.id).subscribe(
          res2 =>{
          this.medicaments =  res2.json().map(medicament => { return {...medicament.medicament,quantity : medicament.quantity}});
          this.filtredMeds = this.medicaments;
          
          this.pharmacieId = res.pharmacie.id;
          } 
        );
      } ,
     err => console.log(err)
   );

    
   }

  ngOnInit() {
    this.getLocation();
  }

  medicamentSearch(value : string) {
    this.filtredMeds = this.medicaments.filter(medicament => medicament.name.toUpperCase().includes(value.toUpperCase()));
   }

  commandeSearch(commandeRef){
    this.filtredCmd = this.commandes.filter(commande => commande.id==commandeRef);
  }

  getLocationByClick(event){
    this.currentLat = event.coords.lat;
    this.currentLng = event.coords.lng;
   }

   private getLocation(){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat= position.coords.latitude;
        this.lng = position.coords.longitude;
      });
    }
  }

  updateStatut(value , commande){
    this.commandeService.updateStatus(value,commande.id).subscribe(
      res => console.log(res),
      err => console.log(err)
  }

  actionOnSubmit(myForm: NgForm){
    let m = myForm.value;

    let med = {
        name: m.med_name ,
        reference : m.med_ref ,
        price : m.med_prix,
    }

    console.log(med.name);

   let qte = m.med_qte;
    this.medicamentsService.postMedicament(med ,this.pharmacieId, qte).subscribe(
      res => {
        document.location.href = "pharmacie/dashboard";
        },
      err => console.log(err)
    );
    document.location.href="pharmacie/dashboard";
  }

  UpdateMedicamentSubmit(MyForm : NgForm , id){
    let form = MyForm.value;

    let medicament = {
        "name": form.med_name,  
        "reference" : form.med_ref,
        "price" : form.prix
    }

    console.log("med object :::: "+medicament.name);
    this.medicamentsService.putMedicament(medicament , id).subscribe(
      res=> console.log(res)
    );

    document.location.href="pharmacie/dashboard";
  }
}
