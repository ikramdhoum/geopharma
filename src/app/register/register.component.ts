import { Component, OnInit } from '@angular/core';
import { GoogleMapsAPIWrapper } from '@agm/core';
import { FormGroup, FormControl, FormArray, NgForm } from '@angular/forms';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  providers :[GoogleMapsAPIWrapper, UsersService],
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  currentLat: number;
  currentLng : number;
  locationChose = false;
  isOn = true;
  registerClientForm : FormGroup;

  constructor(private usersService : UsersService , private router : Router) { }

  ngOnInit() {
    this.getLocation();
    
    this.registerClientForm = new FormGroup(
      {
        'client_lastName' : new FormControl(),
        'client_firstName' : new FormControl(),
        'client_adress' : new FormControl(),
        'client_phone' : new FormControl(),
        'client_login' : new FormControl(),
        'client_pwd' : new FormControl()
      }
    );
  }
  


  

  actionOnSubmit(myForm: NgForm){
    let u = myForm.value;
    let user = {
      username : u.client_login,
       name : u.client_firstName+" "+u.client_lastName ,
       email : u.client_email,
       password : u.client_pwd,
       addresse : u.client_adress,
       phoneNumber : u.client_phone,
       roles : [(this.isOn)?"ROLE_CLIENT":"ROLE_PHARMACIE"],
       pharmacie : null
    }
    this.usersService.signUp(user).subscribe(
      res => {
        this.usersService.setAccessToken(res._body,(this.isOn)?"ROLE_CLIENT":"ROLE_PHARMACIE");
        if(this.isOn) {
          this.router.navigate(['/client/dashboard']);
        }
        else {
          this.router.navigate(['/pharmacie/dashboard']);
        }
      },
      err => console.log(err)
    )
    console.log(user);
  }

  createPharmacie(pharmacieForm : NgForm) {

    let f = pharmacieForm.value;
       let pharmacieUser = {
        username : f.pharmacie_login,
        name : f.pharmacie_name,
        email : f.pharmacie_email , 
        password  : f.pharmacie_password , 
        addresse  : f.pharmacie_addresse,
        phoneNumber : f.pharmacie_tel,
        roles : [(this.isOn)?"ROLE_CLIENT":"ROLE_PHARMACIE"],
        pharmacie : {
          name : f.pharmacie_name,
          addresse  : f.pharmacie_addresse,
          phoneNumber : f.pharmacie_tel,
          gsm : f.pharmacie_gsm, 
          schedule : "Lun au Ven 8h00 à 18hh00",
            latitude : this.currentLat,
            longitude : this.currentLng
        }
       } 
       console.log(pharmacieUser);
this.usersService.signUp(pharmacieUser).subscribe(
      res => {
        this.usersService.setAccessToken(res._body,(this.isOn)?"ROLE_CLIENT":"ROLE_PHARMACIE");
        if(this.isOn) {
          this.router.navigate(['/client/dashboard']);
        }
        else {
          this.router.navigate(['/pharmacie/dashboard']);
        }
      },
      err => console.log(err)
    )
    console.log(pharmacieUser);
  }  
  
  signin(loginForm: NgForm) {
    let value = loginForm.value;
    console.log(value);
    this.usersService.login({username :loginForm.value.username , password : loginForm.value.password}).subscribe(
      res => {
        this.usersService.getProfile(res._body).subscribe(
          res2 => {
            
             this.usersService.setAccessToken(res._body,res2.roles[0]);
            if(res2.roles[0] == "ROLE_CLIENT") {
              this.router.navigate(['/client/dashboard']);
            }
            else {
              this.router.navigate(['/pharmacie/dashboard']);
            }
          }
        )
        
        
      },
      err => console.log(err)
    )
  }

  
  submitClientForm(){
    
  }

  private getLocation(){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.currentLat = position.coords.latitude;
        this.currentLng = position.coords.longitude;
      });
    }
  }

  getLocationByClick(event){
   this.currentLat = event.coords.lat;
   this.currentLng = event.coords.lng;
   this.locationChose = true;
  }

}
