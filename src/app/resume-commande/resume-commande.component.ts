import { Component, OnInit , Input } from '@angular/core';
import { CommandeComponent } from '../commande/commande.component';
import {Router} from "@angular/router";

@Component({
  selector: 'app-resume-commande',
  templateUrl: './resume-commande.component.html',
  styleUrls: ['./resume-commande.component.css']
})
export class ResumeCommandeComponent implements OnInit {

  @Input() cartArticles;

  constructor(private router : Router) {
  }

  ngOnInit() {
  }

  navigateRedirect(){
    this.router.navigate(['/accueil']);
  }


}
