import { TestBed, inject } from '@angular/core/testing';

import { CommandesService } from './commandes.service';

describe('CommandesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommandesService]
    });
  });

  it('should be created', inject([CommandesService], (service: CommandesService) => {
    expect(service).toBeTruthy();
  }));
});
