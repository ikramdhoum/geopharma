import { Injectable , EventEmitter } from '@angular/core';
import { environment } from '../../environments/environment';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
const ENDPOINT = environment.host + "/api/orders";
@Injectable()
export class CommandesService {
  headers;
  
  constructor(private http: Http ) {
    
    
    this.headers = new Headers();
		this.headers.append("Content-Type", "application/json");
		this.headers.append("Authorization", "Bearer " + localStorage.getItem('token'));
		
  }

  checkout(orders) {
    console.log(orders , ENDPOINT);
    return this.http.post(ENDPOINT,{orders},{headers:this.headers}).catch(this.handleError);
  }
  handleError(error) {
    // log Error in a log file or send the error to a third party
    return Observable.throw(error);

  }
  clientOrders() {
    return this.http.get(environment.host + "/api/users/orders", { headers: this.headers } )
    .map(res => res.json().map( order => {return{...order , orderMedicament : order.orderMedicament.map( om => { return {...om , img : environment.host+'/api/files/'+om.medicament.file.name, quantity : om.quantity}} )}})).catch(this.handleError); 
  }
  pharmacieOrders() {
    return this.http.get(environment.host + "/api/orders", { headers: this.headers } )
    .map(res => res.json().map( order => {return{...order , orderMedicament : order.orderMedicament.map( om => { return {...om , img : environment.host+'/api/files/'+om.medicament.file.name, quantity : om.quantity}} )}})).catch(this.handleError); 
  }

  updateStatus(status,id) {
    return this.http.put(environment.host + "/api/orders/"+id ,{status}, {headers : this.headers}).catch(this.handleError);
  }

}
