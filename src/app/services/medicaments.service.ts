import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


const ENDPOINT = environment.host + "/api";
@Injectable()
export class MedicamentsService {

  authChanged: EventEmitter<any> = new EventEmitter();
  headers;
  private API_URL = environment.host+'s';

  handleError(arg0: any): any {
    throw new Error("Method not implemented.");
  }

    constructor(private http: Http, private router : Router ){ 
      
    }

    
    
    public postMedicament(medicament ,pharmacieId ,  qte) : Observable<any> {
      console.log(medicament);
      console.log(pharmacieId);
      console.log(qte);
      // let med = {name : "test" , reference : "124" , price : "4343" , pharmacieId : "3"};
      return this.http.post(ENDPOINT+"/pmedicaments/"+qte,{...medicament , pharmacieId : pharmacieId}).catch(this.handleError);
    
    }

    public putMedicament(medicament , id) : Observable<any> {
      
      this.headers = new Headers();
      this.headers.append("Content-Type", "application/json");
      this.headers.append("Authorization", "Bearer " + localStorage.getItem("token"));
      let url = ENDPOINT+"/medicaments/"+id;
      
      return this.http.put(url ,medicament, {headers : this.headers}).catch(this.handleError);
    }

    public gtMedicamentByPharmacie(pharmacieId) : Observable<any> { 
      let url = ENDPOINT+"/pharmacies/"+pharmacieId+"/medicaments"
      return this.http.get(url).catch(this.handleError);
    }

  

  






}
