import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Headers, Http } from '@angular/http';

import {Observable} from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PharmaciesService {
private    API_URL = environment.host+'/api/pharmacies';

headers;
  constructor(private http: Http ){ 
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append("Accept" ,"application/json");
  }



  public getAllPharmacies() : Observable<any> {  
    return this.http.get(this.API_URL, { headers: this.headers } )
    .map(res => res.json()).catch(this.handleError); 
  }
  public getPharmacie(id) : Observable<any> {  
    return this.http.get(this.API_URL+"/"+id, { headers: this.headers } )
    .map(res => res.json()).catch(this.handleError); 
  }
  public getAllMedicamentsOfPharmacie(id) : Observable<any> {  
    return this.http.get(this.API_URL+"/"+id+"/medicaments", { headers: this.headers } )
    .map(res => res.json().map(Pmedicament => 
      {
        let filename = (Pmedicament.medicament.file!==null)?Pmedicament.medicament.file.name:"test";
        return{... Pmedicament.medicament , img : environment.host+'/api/files/'+filename, quantity : Pmedicament.quantity}})).catch(this.handleError); 
  }

  private handleError(error: any): Observable<any> {
    console.log(error);
    
    if(error.json().$error!=undefined) {
    return Observable.throw(error.json().$error);
    }
    else {
      
        return Observable.throw(error.json().reasonPhrase);
    }
  
  }
}
