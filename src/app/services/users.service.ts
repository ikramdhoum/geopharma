import { Injectable , EventEmitter } from '@angular/core';
import { environment } from '../../environments/environment';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Router } from '@angular/router';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

const ENDPOINT = environment.host + "/api/users"
@Injectable()
export class UsersService {
	authChanged: EventEmitter<any> = new EventEmitter();
	headers;
  
  constructor(private http: Http , private router : Router) {

		
	 }
   
  login(user) {
    return this.http.post(ENDPOINT+"/signin",user).catch(this.handleError);
  }
  
  setAccessToken(token , role){
    localStorage.setItem('token', token);
    localStorage.setItem('role', role);
		
		this.authChanged.emit(true);
		this.router.navigate(['/home']);
	
	} 

	isAuthenticated() {
		return localStorage.getItem("token")?true:false;
	}
	
	logout() {
		localStorage.removeItem('token');
		localStorage.removeItem('role');
		this.authChanged.emit(false);
		this.router.navigate(['/']);
	}

	signUp(user) {
    return this.http.post(ENDPOINT+"/signup",{...user}).catch(this.handleError);
	}
  handleError(error) {
    // log Error in a log file or send the error to a third party
    return Observable.throw(error);

	}
	getProfile(token) {
		this.headers = new Headers();
		this.headers.append("Content-Type", "application/json");
		this.headers.append("Authorization", "Bearer " + token);
		return this.http.get(ENDPOINT+"/profile", { headers: this.headers } )
    .map(res => res.json()).catch(this.handleError); 
	}

	updateProfile(user) {
		this.headers = new Headers();
		this.headers.append("Content-Type", "application/json");
		this.headers.append("Authorization", "Bearer " + localStorage.getItem('token'));
		return this.http.put(ENDPOINT+"/profile" , user, { headers: this.headers } )
    .map(res => res.json()).catch(this.handleError); 
		
	}
}
