import { Component, OnInit, ViewChild } from '@angular/core';
import { PanierComponent } from '../panier/panier.component';

@Component({
  selector: 'app-shop-checkout',
  templateUrl: './shop-checkout.component.html',
  styleUrls: ['./shop-checkout.component.css']
})
export class ShopCheckoutComponent implements OnInit {

  @ViewChild(PanierComponent) panier;

  constructor() {
   }

   cartArticlesList;

  ngOnInit() {
    this.cartArticlesList = this.panier.cartArticles;
  }

}
